# Social Auth Service API - User Guide

## Last update: 24 July, 2018

## Base url: http://157.7.242.91:8021

---

Currently Supported Providers:

| Provider | <provider_name> | Status |
|---|---|---|
| Github | github | Complete |
| Twitter | twitter | Complete |
| Linkedin | linkedin-oauth2 | Complete |
| Google | google-oauth2 | Complete - Domain name needed |
| Facebook | facebook | Complete - HTTPS needed |

---
## Social Auth Login
### HTTP REQUEST :  **GET  /auth/login/<provider_name>/**

Best way to use this api to load this url in window popup. Sample code is given below:

```javascript
function open_social_windows(url){
    window.open (url,"social_window", 'status=yes,toolbar=no,menubar=no,location=no"');
    var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    var eventer = window[eventMethod];
    var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
    // Listen to message from child window
    eventer(messageEvent,function(e) {
        console.log(e.data);
    }, false);
}
```


#### params
```json 
{

}
```

#### output (browser response)
``` json 
{
    ​"data": { 
        "access_token": "234714212-XS819PfET8LLlwC3x7bkxUlFD2vJGQjiSmKUy57l", 
        "provider": "twitter"
        }
}
```

---

## Get user profile data by access_token and provider
### HTTP REQUEST :  **GET  /auth-user/**

#### params
```json 
{
    "access_token": "234714212-XS819PfET8LLlwC3x7bkxUlFD2vJGQjiSmKUy57l", 
    "provider": "twitter"
}
```

#### output
``` json 
{
    "id": 6,
    "profile_id": "8761194",
    "avatar_url": "https://avatars3.githubusercontent.com/u/8761194?v=4",
    "name": "Md. Asaduzzaman",
    "email": "asaduzzaman.cse@gmail.com",
    "provider": "github",
    "access_token": "eaf2ee2f03346d9c20447d65441e89ee7c8046d2"
}
```

---