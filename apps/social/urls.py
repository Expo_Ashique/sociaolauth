from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path(r'^auth/', include('rest_framework_social_oauth2.urls')),
]
