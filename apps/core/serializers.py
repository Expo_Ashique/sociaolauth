from rest_framework import serializers
from .models import SocialInfo
from django.conf import settings



class SocialInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = SocialInfo
        fields = ('__all__')

