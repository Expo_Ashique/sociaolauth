from django.shortcuts import render, HttpResponse
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
import json
from .models import *
from .serializers import *
from django.contrib.auth.decorators import login_required


@login_required
def home(request):
	return render(request, 'core/home.html',{"access_token":request.session['user_profile']["access_token"], "provider":request.session['user_profile']['provider']})

def _generate_social_profile(request, uid, response):
	if request.session['provider'] == 'github':
		user_profile={
			"profile_id" : uid,
			"avatar_url" : response.get('avatar_url', ''),
			"name" : response.get('name', ''),
			"email" : response.get('email', ''),
			"provider": request.session['provider'],
			'access_token': response.get('access_token', '')
		}
	elif request.session['provider'] == 'twitter':
		access_token = response.get('access_token', '')
		user_profile={
			"profile_id" : uid,
			"avatar_url" : response.get('profile_image_url', ''),
			"name" : response.get('name', ''),
			"email" : response.get('email', ''),
			"provider": request.session['provider'],
			'access_token': access_token.get("oauth_token",'')
		}
	elif request.session['provider'] == 'linkedin-oauth2':
		user_profile={
			"profile_id" : uid,
			"avatar_url" : response.get('pictureUrl', ''),
			"name" : response.get('firstName', '') + " " + response.get('lastName', ''),
			"email" : response.get('emailAddress', ''),
			"provider": request.session['provider'],
			'access_token': response.get('access_token', '')
		}
	elif request.session['provider'] == 'facebook':
		picture_data = response.get('picture', '')
		# print("picture_data", picture_data)
		data_url = picture_data.get("data","")
		# print("data_url", data_url)
		user_profile={
			"profile_id" : uid,
			"avatar_url" : data_url.get('url', ''),
			"name" : response.get('name', ''),
			"email" : response.get('email', ''),
			"provider": request.session['provider'],
			'access_token': response.get('access_token', '')
		}
	else:
		user_profile={
			"profile_id" : '',
			"avatar_url" : '',
			"name" : '',
			"email" : '',
			"provider": '',
			'access_token': ''
		}

	return user_profile


def save_profile(request, backend, uid, response, details, *args, **kwargs):
	print("uid", uid)
	print(backend.name)
	print(json.dumps(response, indent=4))
	
	

	request.session['provider'] = backend.name
	# social = request.user.social_auth.get(provider=request.session['provider'])

	# save profile into database
	user_profile = _generate_social_profile(request, uid, response)

	
	request.session['user_profile']= user_profile

	# check in the data already exist in system
	db_data = SocialInfo.objects.filter(profile_id=request.session['user_profile']["profile_id"],provider=request.session['user_profile']["provider"]).first()

	if db_data is None:
		SocialInfo.objects.create(**user_profile)
	else:
		SocialInfo.objects.filter(pk=db_data.id).update(**user_profile)


	print(json.dumps(request.session['user_profile'], indent=4))


class AuthUser(APIView):
	def get(self, request, format=None):
		access_token = request.GET.get("access_token", None)
		provider = request.GET.get("provider", None)

		if access_token is not None and provider is not None:
			print(access_token, provider)

			db_data = SocialInfo.objects.filter(access_token=access_token,provider=provider).first()
			serializer = SocialInfoSerializer(db_data)

			if db_data is not None:
				return Response(serializer.data, status=status.HTTP_200_OK)
			else:
				return Response(None, status=status.HTTP_404_NOT_FOUND)
		else:
			return Response(None, status=status.HTTP_400_BAD_REQUEST)


