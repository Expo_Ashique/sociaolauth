from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_view
from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', auth_view.login, name='login'),
    url(r'^logout/$', auth_view.logout, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
    url(r'^$', views.home, name='home'),
    url(r'^auth-user/$', views.AuthUser.as_view(), name='auth_user'),
]
