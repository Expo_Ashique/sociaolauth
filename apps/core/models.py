from django.db import models
from datetime import datetime

# Create your models here.
class SocialInfo(models.Model):
    profile_id = models.CharField(max_length=255, default="")
    avatar_url = models.CharField(max_length=255, default="")
    name = models.CharField(max_length=255, default="")
    email = models.CharField(max_length=255, default="")
    provider = models.CharField(max_length=255, default="")
    access_token = models.TextField(default="")
