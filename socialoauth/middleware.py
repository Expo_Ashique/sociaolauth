from datetime import datetime
from django.utils.deprecation import MiddlewareMixin
import logging
import json
from django.conf import settings
import os
import time
import datetime
from django.http import QueryDict

logger = None

class LoggingMiddleware(MiddlewareMixin):
    def _init(self, dirpath, filename):
        """init."""
        self.dirpath = dirpath
        self.logpath = dirpath + "/" + filename
        # self.init_log_env()
        self.log_fp = open(self.logpath, "a+")

    def init_log_env(self):
        """Initialize Log Env."""
        if not os.path.exists(self.logpath):
            os.mkdir(self.dirpath)
        return

    def finish_log(self):
        """Finish Log."""
        self.log_fp.close()
        return

    def log(self, logtype, url, logstring, hashstring=""):
        # timestamp = int(time.time())
        
        now=datetime.datetime.now()
        timestamp=now.strftime("%d/%b/%Y %H:%M:%S.%f")

        self.log_fp.write("[" + str(timestamp) + "]\t" + str(logtype) + "\t" + str(url) + "\t" + str(logstring) + "\t" + str(hashstring) + "\n")
        self.log_fp.flush()
        return

    def process_request(self, request):
        REQUEST_METHOD=request.method

        if request.method == "GET":
            REQUEST_DATA=request.GET
        else:
            REQUEST_DATA=QueryDict(request.body)

        enabled = getattr(settings, 'LOG_ENABLED', False)
        logdir = getattr(settings, 'LOG_DIR', False)
        if not enabled:
            return

        now=datetime.datetime.now()
        filename=now.strftime("%Y-%m-%d_%H-00")
        self._init(logdir,filename+".log")

        self.log(REQUEST_METHOD,request.build_absolute_uri(),json.dumps(REQUEST_DATA))
